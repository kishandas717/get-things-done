const { app, BrowserWindow } = require('electron');

if (app.isFirstRun) {
    localStorage.clear();
}

let win;

function createWindow() {
    win = new BrowserWindow({
        width: 1400,
        height: 800,
        webPreferences: {
            nodeIntegration: true,
            allowRunningInsecureContent: false,
            contextIsolation: false,
            enableRemoteModule: true,
            webSecurity: true
        }
    });

    win.loadURL(`file://${__dirname}/dist/ng-todo-hub/index.html`)

    // Uncomment the following line to open the DevTools.
    // win.webContents.openDevTools();

    win.on('closed', () => {
        win = null;
    });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (win === null) {
        createWindow();
    }
});