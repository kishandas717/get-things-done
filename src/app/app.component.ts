import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'NG-TodoHub';
  menus = [
    {
      label: 'TASKS',
      children: [
        { label: 'Todo', icon: 'bi bi-list-task', link: '/todos' },
        { label: 'Calendar', icon: 'bi bi-calendar4-week', link: '/calendar' },
        { label: 'Events', icon: 'bi bi-calendar2-event', link: '/events'},
        { label: 'Sticky Notes', icon: 'bi bi-journals', link: '/notes'},
      ]
    },
    {
      label: 'IDENTIFIERS',
      children: [
        { label: 'Labels', icon: 'bi bi-bookmarks', link: '/labels' },
        { label: 'Tags', icon: 'bi bi-tags', link: '/tags' },
        { label: 'Priorities', icon: 'bi bi-1-square', link: '/priorities'},
        { label: 'Milestones', icon: 'bi bi-signpost-split', link: '/milestones'},
      ]
    }
  ];
}
