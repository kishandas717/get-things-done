import { Component, OnInit } from '@angular/core';
import { CustomFormInput, CustomFormSetting } from 'src/app/shared/custom-collapse/custom-collapse.model';
import { CustomTableSetting } from 'src/app/shared/custom-table/custom-table.model';

@Component({
  selector: 'app-labels',
  templateUrl: './labels.component.html',
  styleUrls: ['./labels.component.scss']
})
export class LabelsComponent implements OnInit {

  labelFormSetting!: CustomFormSetting;
  labelTableSetting!: CustomTableSetting;
  labelFormInputs: CustomFormInput[] = [];
  
  constructor() {}

  ngOnInit(): void {

    this.labelFormSetting = {
      name: 'labels',
      saveBtn: 'Create Label',
      cancelBtn: 'Cancel',
      collapseBtn: 'New Label'
    };

    this.labelFormInputs = [
      {
        label: 'Name',
        type: 'text',
        class: [],
        controlName: 'name',
        placeholder: 'Name',
        value: '',
        disabled: false,
        hidden: false
      },
      {
        label: 'Description',
        type: 'text',
        class: [],
        controlName: 'description',
        placeholder: 'Description',
        value: '',
        disabled: false,
        hidden: false
      },
      {
        label: 'Color',
        type: 'color',
        class: ['color-input'],
        controlName: 'color',
        placeholder: '',
        value: '#845ef7',
        disabled: false,
        hidden: false
      }
    ];

    this.labelTableSetting = {
        type: 'labels',
        columnDef: [
          { name: 'Preview', propName: '' },
          { name: 'Name', propName: 'name' },
          { name: 'Description', propName: 'description' },
          { name: 'Color', propName: 'color' }
        ]
    };
  }

}
