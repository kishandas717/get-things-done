import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Events } from './events.model';
import { EventsService } from './events.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
  
  events: Events[] = [];
  singleEvent: Events = {};
  isEdit!: boolean;

  constructor(private eventsService: EventsService) {}

  ngOnInit() {
    this.events = this.eventsService.getEvents();    
  }

  openOffCanvas(isEdit: boolean, event: Events = {}) {
    this.isEdit = isEdit;
    this.singleEvent = event;
  }

  deleteEvent(event: Events) {
    this.eventsService.deleteEvent(event);
  }
}
