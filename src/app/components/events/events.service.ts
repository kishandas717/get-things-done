import { Injectable } from '@angular/core';
import { Events } from './events.model';


@Injectable({
    providedIn: 'root',
})

export class EventsService {
    private events: Events[] = [];

    constructor() {
        this.events = JSON.parse(localStorage.getItem('events')!) || [];
    }

    addEvent(event: Events) {
        this.events.push(event);
        localStorage.setItem('events', JSON.stringify(this.events));
    }

    getEvents() {
        return this.events;
    }

    updateEvent(event: Events) {
        const index = this.events.findIndex((e) => e.id === event.id);
        this.events[index] = event;
        localStorage.setItem('events', JSON.stringify(this.events));
    }

    deleteEvent(event: Events) {
        const index = this.events.findIndex((e) => e.id === event.id);
        this.events.splice(index, 1);
        localStorage.setItem('events', JSON.stringify(this.events));
    }
}