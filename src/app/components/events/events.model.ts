export class Events {
    id?: string;
    title?: string;
    description?: string; 
    start?: string;
    end?: string;
}

export type EventDate = {
    day?: number,
    month?: number,
    year?: number
}

export type EventTime = {
    hour?: number,
    minute?: number
}