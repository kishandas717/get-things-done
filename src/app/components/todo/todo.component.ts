import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { TodoFormComponent } from './todo-form/todo-form.component';
import { Todo } from './todo.model';
import { TodoService } from './todo.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {

  @ViewChild('container', { read: ViewContainerRef, static: true }) container!: ViewContainerRef;

  component!: any;
  todos: Todo[] = [];

  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
    this.todos = this.todoService.getTodos();
  }

  createComponent(isEdit: boolean, todo: Todo = {}) {
    if (!this.component || isEdit) {
      this.container.clear();
      this.component = this.container.createComponent(TodoFormComponent);
      this.component.instance.todo = todo;
      this.component.instance.isEdit = isEdit;

      this.component.instance.closeTodoEmitter.subscribe((event: any) => {
        this.component.destroy();
        this.component = null;
      })
    }
  }

  todoHandler(todo: Todo) {
    this.createComponent(true, todo);
  }
}
