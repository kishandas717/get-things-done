export interface Todo {
  id?: string;
  summary?: string;
  status?: Status;
  priority?: string;
  label?: string;
  tag?: string;
}

export enum Status {
  Not_Started= 'Not Started',
  In_Progress= 'In Progress',
  Completed= 'Completed'
}
