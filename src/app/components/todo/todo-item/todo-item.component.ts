import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Todo } from '../todo.model';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent {
  @Input()
  todo!: Todo;

  @Output() sendTodo = new EventEmitter<Todo>();

  constructor() {}

  todoHandler() {
    this.sendTodo.emit(this.todo);
  }

  getStatusClass() {
    let className;
    switch(this.todo.status) {
      case 'Not Started':
        className = 'red-bg';
        break;
      case 'In Progress':
        className = 'blue-bg';
        break;
      case 'Completed':
        className = 'green-bg';
        break;
      default:
        className = '';
        break;
    }
    return className;
  }
}
