import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Status, Todo } from '../todo.model';
import { TodoService } from '../todo.service';
import { v4 as uuidv4 } from 'uuid';

import { trigger, transition, style, animate, group, state } from '@angular/animations';
import { CustomCollapseService } from 'src/app/shared/custom-collapse/custom-collapse.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.scss'],
  animations: [
    trigger('fadeInOut', [
      state('in', style({ opacity: 1, transform: 'translateY(0)' })),
      transition('void => *', [
        style({ opacity: 0, transform: 'translateY(100%)' }),
        animate(500)
      ]),
      transition('* => void', [
        animate(500, style({ opacity: 0, transform: 'translateY(100%)' }))
      ])
    ])
  ]
})
export class TodoFormComponent implements OnInit {
  @Input() todo: Todo = {};
  @Input() isEdit: boolean = false;
  @Output() closeTodoEmitter = new EventEmitter<void>();
  @ViewChild('todoForm', { static: true }) todoForm!: NgForm;

  status = Status;
  formTitle!: string;
  labels: any[] = [];
  tags: any[] = [];
  priorities: any[] = [];

  constructor(private todoService: TodoService, private customService: CustomCollapseService) {
    
  }

  ngOnInit(): void {
    this.formTitle = this.isEdit ? 'Edit Task' : 'New Task';
    this.labels = this.customService.getData('labels');
    this.tags = this.customService.getData('tags');
    this.priorities = this.customService.getData('priorities');
  }

  saveTodo() {
    if (!this.isEdit) {
      this.createTodo();
    } else {
      this.editTodo();
    }
    this.closeTodo();
  }

  createTodo() {
    this.todo.id = uuidv4();
    this.todoService.addTodo(this.todo);
  }

  editTodo() {
    this.todoService.updateTodo(this.todo)
  }

  deleteTodo(todo: Todo) {
    this.todoService.deleteTodo(todo);
    this.closeTodo();
  }

  closeTodo() {
    this.closeTodoEmitter.emit();
  }
}
