import { Component, OnInit } from '@angular/core';
import { CustomFormInput, CustomFormSetting } from 'src/app/shared/custom-collapse/custom-collapse.model';
import { CustomTableSetting } from 'src/app/shared/custom-table/custom-table.model';

@Component({
  selector: 'app-milestones',
  templateUrl: './milestones.component.html',
  styleUrls: ['./milestones.component.scss']
})
export class MilestonesComponent implements OnInit {

  milestoneFormSetting!: CustomFormSetting;
  milestoneTableSetting!: CustomTableSetting;
  milestoneFormInputs: CustomFormInput[] = [];

  ngOnInit(): void {

    this.milestoneFormSetting = {
      name: 'milestones',
      saveBtn: 'Create Milestone',
      cancelBtn: 'Cancel',
      collapseBtn: 'New Milestone'
    };

    this.milestoneFormInputs = [
      {
        label: 'Name',
        type: 'text',
        class: [],
        controlName: 'name',
        placeholder: 'Name',
        value: '',
        disabled: false,
        hidden: false
      },
      {
        label: 'Description',
        type: 'text',
        class: [],
        controlName: 'description',
        placeholder: 'Description',
        value: '',
        disabled: false,
        hidden: false
      },
      {
        label: 'Date',
        type: 'date',
        class: [],
        controlName: 'date',
        placeholder: 'Date',
        value: '',
        disabled: false,
        hidden: false
      },
      {
        label: 'Color',
        type: 'color',
        class: ['color-input'],
        controlName: 'color',
        placeholder: '',
        value: '#845ef7',
        disabled: false,
        hidden: false
      }
    ];

    this.milestoneTableSetting = {
      type: 'milestones',
      columnDef: [
        { name: 'Preview', propName: '' },
        { name: 'Name', propName: 'name' },
        { name: 'Description', propName: 'description' },
        { name: 'Date', propName: 'date' },
        { name: 'Color', propName: 'color' }
      ]
    };
  }
}
