import { Component, OnInit } from '@angular/core';
import { CustomFormInput, CustomFormSetting } from 'src/app/shared/custom-collapse/custom-collapse.model';
import { CustomTableSetting } from 'src/app/shared/custom-table/custom-table.model';

@Component({
  selector: 'app-priorities',
  templateUrl: './priorities.component.html',
  styleUrls: ['./priorities.component.scss']
})
export class PrioritiesComponent implements OnInit {

  priorityFormSetting!: CustomFormSetting;
  priorityTableSetting!: CustomTableSetting;
  priorityFormInputs: CustomFormInput[] = [];

  ngOnInit(): void {

    this.priorityFormSetting = {
      name: 'priorities',
      saveBtn: 'Create Priority',
      cancelBtn: 'Cancel',
      collapseBtn: 'New Priority'
    };

    this.priorityFormInputs = [
      {
        label: 'Name',
        type: 'text',
        class: [],
        controlName: 'name',
        placeholder: 'Name',
        value: '',
        disabled: false,
        hidden: false
      },
      {
        label: 'Description',
        type: 'text',
        class: [],
        controlName: 'description',
        placeholder: 'Description',
        value: '',
        disabled: false,
        hidden: false
      },
      {
        label: 'Color',
        type: 'color',
        class: ['color-input'],
        controlName: 'color',
        placeholder: '',
        value: '#845ef7',
        disabled: false,
        hidden: false
      }
    ];

    this.priorityTableSetting = {
      type: 'priorities',
      columnDef: [
        { name: 'Preview', propName: '' },
        { name: 'Name', propName: 'name' },
        { name: 'Description', propName: 'description' },
        { name: 'Color', propName: 'color' }
      ]
    };
  }
}
