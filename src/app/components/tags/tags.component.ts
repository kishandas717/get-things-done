import { Component, OnInit } from '@angular/core';
import { CustomFormSetting, CustomFormInput } from 'src/app/shared/custom-collapse/custom-collapse.model';
import { CustomTableSetting } from 'src/app/shared/custom-table/custom-table.model';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent implements OnInit {

  tagFormSetting!: CustomFormSetting;
  tagTableSetting!: CustomTableSetting;
  tagFormInputs: CustomFormInput[] = [];
  
  constructor() {}

  ngOnInit(): void {

    this.tagFormSetting = {
      name: 'tags',
      saveBtn: 'Create Tag',
      cancelBtn: 'Cancel',
      collapseBtn: 'New Tag'
    };

    this.tagFormInputs = [
      {
        label: 'Name',
        type: 'text',
        class: [],
        controlName: 'name',
        placeholder: 'Name',
        value: '',
        disabled: false,
        hidden: false
      },
      {
        label: 'Color',
        type: 'color',
        class: ['color-input'],
        controlName: 'color',
        placeholder: '',
        value: '#845ef7',
        disabled: false,
        hidden: false
      }
    ];

    this.tagTableSetting = {
        type: 'tags',
        columnDef: [
          { name: 'Preview', propName: '' },
          { name: 'Name', propName: 'name' },
          { name: 'Color', propName: 'color' }
        ]
    }
  }
}
