import { Injectable } from "@angular/core";
import { Note } from "./notes.model";

@Injectable({
    providedIn: 'root'
})
export class NotesService {
    private notes: Note[] = [];

    constructor() {
        this.notes = JSON.parse(localStorage.getItem('notes')!) || [];
    }

    addNote(note: Note) {
        this.notes.push(note);
        this.notes= this.notes.sort((a,b)=>{ return b.id!-a.id!});
        localStorage.setItem('notes', JSON.stringify(this.notes));
    }

    getNotes() {
        return this.notes;
    }

    updateNote(note: Note) {
        const index = this.notes.findIndex((n) => n.id === note.id);
        this.notes[index] = note;
        localStorage.setItem('notes', JSON.stringify(this.notes));
    }

    deleteNote(note: Note) {
        const index = this.notes.findIndex((n) => n.id === note.id);
        this.notes.splice(index, 1);
        localStorage.setItem('notes', JSON.stringify(this.notes));
    }
}