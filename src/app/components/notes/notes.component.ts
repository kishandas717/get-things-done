import { Component, OnInit } from '@angular/core';
import { NotesService } from './notes.service';
import { Note } from './notes.model';
import { getRandomBgColor } from '../../utils/generic.utils';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit {

  titleStyle = {
    'font-weight': 'bold',
    'border-radius': '5px',
    'padding': '5px 5px'
  }

  contentStyle = {
    'border-radius': '5px',
    'padding': '5px 5px',
    'height': '150px',
    'word-break': 'break-all'
  }

  notes: Note[] = [];

  constructor(private notesService: NotesService) { }

  ngOnInit(): void {
    this.notes = this.notesService.getNotes();
  }

  addNoteTemplate() {
    const notesLength = this.notes.length;
    const note: Note = {
      id: notesLength + 1,
      title: 'Title...',
      content: 'Content...',
      bgColor: getRandomBgColor()
    }
    this.notesService.addNote(note);
  }

  updateNote(mouseEvent: MouseEvent) {
    console.log(mouseEvent);
    const targetElement = (mouseEvent.target! as HTMLElement);
    const id = +targetElement.parentElement?.parentElement?.parentElement?.parentElement?.parentElement?.getAttribute('id')!;
    const bgColor = targetElement.parentElement?.parentElement?.style?.backgroundColor;
    let title: string;
    let content: string;
    if (targetElement.tagName === 'H5') {
      title = targetElement.innerText;
      content = (targetElement?.nextSibling as HTMLElement).innerText;
    } else {
      content = targetElement.innerText;
      title = (targetElement?.previousSibling as HTMLElement).innerText;
    }

    const note: Note = {
      id,
      title,
      content,
      bgColor
    }

    this.notesService.updateNote(note);
    
  }

  deleteNote(mouseEvent: MouseEvent) {
    const id = +(mouseEvent.target! as HTMLElement).parentElement?.parentElement?.parentElement?.parentElement?.parentElement?.getAttribute('id')!;
    const note = this.notes.filter(note => note.id === id);
    this.notesService.deleteNote(note[0]);
  }

}
