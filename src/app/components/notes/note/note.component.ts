import { Component, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent {
  @Input() color!: string;
  @Output() dismiss = new EventEmitter();
  @Output() focusout = new EventEmitter();

  constructor() {}

  onDismiss(mouseEvent: MouseEvent){
    this.dismiss.emit(mouseEvent);
  }
  
  onFocusOut(mouseEvent: MouseEvent){
    this.focusout.emit(mouseEvent);
  }
}
