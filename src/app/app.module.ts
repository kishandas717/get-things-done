import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { MatSidenavModule } from '@angular/material/sidenav';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FullCalendarModule } from '@fullcalendar/angular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material/material.module';
import { NgbCollapseModule, NgbPaginationModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';

import { TodoComponent } from './components/todo/todo.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { TodoItemComponent } from './components/todo/todo-item/todo-item.component';
import { TodoFormComponent } from './components/todo/todo-form/todo-form.component';
import { NotesComponent } from './components/notes/notes.component';
import { EventsComponent } from './components/events/events.component';
import { SidePanelComponent } from './shared/side-panel/side-panel.component';
import { TagsComponent } from './components/tags/tags.component';
import { NoteComponent } from './components/notes/note/note.component';
import { LabelsComponent } from './components/labels/labels.component';
import { CustomTableComponent } from './shared/custom-table/custom-table.component';
import { PrioritiesComponent } from './components/priorities/priorities.component';
import { MilestonesComponent } from './components/milestones/milestones.component';
import { CustomCollapseComponent } from './shared/custom-collapse/custom-collapse.component';

import { ContenteditableMaxLengthDirective } from './directives/content-editable-max-length.directive';
import { SortableDirective } from './directives/sortable.directive';

import { SearchPipe } from './pipes/search-pipe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    CalendarComponent,
    TodoItemComponent,
    TodoFormComponent,
    NotesComponent,
    EventsComponent,
    SidePanelComponent,
    TagsComponent,
    NoteComponent,
    ContenteditableMaxLengthDirective,
    SortableDirective,
    LabelsComponent,
    CustomTableComponent,
    PrioritiesComponent,
    MilestonesComponent,
    CustomCollapseComponent,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatSidenavModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    FullCalendarModule,
    NgbModule,
    CommonModule,
    NgbCollapseModule,
    NgbPaginationModule,
    NgbTypeaheadModule
  ],
  providers: [DecimalPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
