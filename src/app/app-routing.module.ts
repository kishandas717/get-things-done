import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalendarComponent } from './components/calendar/calendar.component';
import { TodoComponent } from './components/todo/todo.component';
import { NotesComponent } from './components/notes/notes.component';
import { EventsComponent } from './components/events/events.component';
import { LabelsComponent } from './components/labels/labels.component';
import { TagsComponent } from './components/tags/tags.component';
import { PrioritiesComponent } from './components/priorities/priorities.component';
import { MilestonesComponent } from './components/milestones/milestones.component';

const routes: Routes = [
  { path: 'todos', component: TodoComponent },
  { path: 'calendar', component: CalendarComponent },
  { path: 'notes', component: NotesComponent},
  { path: 'events', component: EventsComponent},
  { path: 'labels', component: LabelsComponent},
  { path: 'tags', component: TagsComponent},
  { path: 'priorities', component: PrioritiesComponent},
  { path: 'milestones', component: MilestonesComponent},
  { path: '', redirectTo: '/todos', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
