import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CustomCollapseService {
  private customData: any = [];
  public shareData: Subject<void>;

  constructor() {
    this.shareData = new Subject();
  }

  addData(data: any, type: string) {
    this.customData.push(data);
    localStorage.setItem(type, JSON.stringify(this.customData));
  }

  getData(type: string) {
    return this.customData = JSON.parse(localStorage.getItem(type)!) || [];
  }

  updateData(data: any, type: string) {
    const index = this.customData.findIndex((d: any) => d.id === data.id);
    this.customData[index] = data;
    localStorage.setItem(type, JSON.stringify(this.customData));
  }

  deleteData(data: any, type: string) {
    const index = this.customData.findIndex((d: any) => d.id === data.id);
    this.customData.splice(index, 1);
    localStorage.setItem(type, JSON.stringify(this.customData));
  }
}
