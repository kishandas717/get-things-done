import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomFormInput, CustomFormSetting } from './custom-collapse.model';
import { CustomCollapseService } from './custom-collapse.service';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-custom-collapse',
  templateUrl: './custom-collapse.component.html',
  styleUrls: ['./custom-collapse.component.scss']
})
export class CustomCollapseComponent implements OnInit {

  @Input() formSetting!: CustomFormSetting;
  @Input() formInputs!: CustomFormInput[];

  customForm!: FormGroup;
  isCollapsed = false;

  constructor(private fb: FormBuilder, private customService: CustomCollapseService) {}

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    let controls: any = {};
    this.formInputs.forEach((input) => {
      if (input) {
        const controlName = input.controlName;
        controls[controlName!] = [input.value, input.controlName !== 'color' ? Validators.required : ''];
      }
    });
    this.customForm = this.fb.group(controls);
  }

  patchValue(form: FormGroup) {
    let controls: any = {};
    this.formInputs.forEach((input) => {
      if (input) {
        const controlName = input.controlName;
        controls[controlName!] = input.value;
      }
    });
    form.patchValue(controls);
  }

  onSubmit(form: FormGroup) {
    const formData = {
      id: uuidv4(),
      ...form.value
    };
    const type = this.formSetting.name;
    this.customService.addData(formData, type!);
    this.customService.shareData.next();
    this.isCollapsed = true;
    this.patchValue(form);
  }
}
