export interface CustomFormSetting {
    name?: string;
    saveBtn?: string;
    cancelBtn?: string;
    collapseBtn?: string;
}

export interface CustomFormInput {
    label?: string;
    type?: string;
    class?: Array<string>;
    controlName?: string;
    placeholder?: string;
    value?: string;
    disabled?: boolean;
    hidden?: boolean;
}