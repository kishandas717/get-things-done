import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, Renderer2, SimpleChanges, ViewChild } from '@angular/core';
import { Events, EventDate, EventTime } from 'src/app/components/events/events.model';
import { NgbDate, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { v4 as uuidv4 } from 'uuid';
import { EventsService } from 'src/app/components/events/events.service';
import { NgForm } from '@angular/forms';
import { Offcanvas } from 'bootstrap';

@Component({
  selector: 'app-side-panel',
  templateUrl: './side-panel.component.html',
  styleUrls: ['./side-panel.component.scss']
})
export class SidePanelComponent implements OnChanges, OnInit, AfterViewInit {
  @ViewChild('offcanvas', { static: true }) offcanvas!: ElementRef;
  @ViewChild('eventForm', { static: true }) evenForm!: NgForm;

  @Input() isEdit: boolean = false;
  @Input() eventDetails: Events = {};

  offCanvasComponent!: Offcanvas;
  offCanvasTitle!: string;
  events: Events = {};

  startDate!: EventDate;
  endDate!: EventDate;
  startTime!: EventTime;
  endTime!: EventTime;

  constructor(private renderer: Renderer2, private eventService: EventsService) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['isEdit']) {
      this.isEdit = changes['isEdit'].currentValue;
    }
    const { eventDetails } = changes;
    this.events = eventDetails.currentValue;
    this.startDate = this.getDateTime(new Date(this.events.start!)).eventDate;
    this.endDate = this.getDateTime(new Date(this.events.end!)).eventDate;
    this.startTime = this.getDateTime(new Date(this.events.start!)).eventTime;
    this.endTime = this.getDateTime(new Date(this.events.end!)).eventTime;
    this.offCanvasTitle = this.isEdit ? 'Edit Event' : 'Add New Event';
  }

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.offCanvasComponent = new Offcanvas(this.offcanvas.nativeElement);
    this.renderer.listen(this.offcanvas.nativeElement, 'click', (event) => {
      event.stopPropagation();
    });
  }

  onDateSelect(event: NgbDate) {
    return;
  }

  setDateTime(eventDate: EventDate, eventTime: EventTime) {
    const { year, month, day } = eventDate;
    const { hour, minute } = eventTime;
    const eventDateTime = new Date(year!, month! - 1, day, hour, minute).toISOString();
    return eventDateTime;
  }

  getDateTime(dateTime: Date) {
    const year = dateTime.getFullYear();
    const month = dateTime.getMonth();
    const date = dateTime.getDate();
    const hour = dateTime.getHours();
    const minute = dateTime.getMinutes();

    const eventDate = {
      day: date,
      month: month,
      year: year
    }

    const eventTime = {
      hour: hour,
      minute: minute
    }

    return { eventDate, eventTime }
  }

  saveEvent() {
    const eventData: Events = {
      id: this.isEdit ? this.events.id : uuidv4(),
      title: this.events.title,
      description: this.events.description,
      start: this.setDateTime(this.startDate, this.startTime),
      end: this.setDateTime(this.endDate, this.endTime)
    }
    if(this.isEdit) {
      this.editEvent(eventData);
    } else {
      this.addEvent(eventData);
    }
  }

  addEvent(eventData: Events) {
    this.eventService.addEvent(eventData);
  }

  editEvent(eventData: Events) { 
    this.eventService.updateEvent(eventData)
  }
}
