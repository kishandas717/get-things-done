export interface CustomTableSetting {
    type?: string;
    columnDef?: ColumnDef[];
}

export interface ColumnDef {
    name?: string;
    propName?: string;
    styles?: Style;
}

export interface TableData {
    [key: string]: any;
}

export interface Style {
    [key: string]: string;
}

export interface SearchResult {
    tableData: TableData[];
    count: number;
}

export interface State {
	page: number;
	pageSize: number;
	searchTerm: string;
	sortColumn: SortColumn;
	sortDirection: SortDirection;
}

export const IDENTIFIER = {
    LABELS: 'labels',
    TAGS: 'tags',
    PRIORITIES: 'priorities',
    MILESTONES: 'milestones'
}

export type SortColumn = any | '';
export type SortDirection = 'asc' | 'desc' | '';