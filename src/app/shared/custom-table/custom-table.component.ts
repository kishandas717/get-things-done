import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { CustomTableSetting, IDENTIFIER, TableData } from './custom-table.model';
import { CustomCollapseService } from '../custom-collapse/custom-collapse.service';
import { CustomFormInput } from '../custom-collapse/custom-collapse.model';
import { SortEvent, SortableDirective } from 'src/app/directives/sortable.directive';
import { Observable, map, startWith, withLatestFrom} from 'rxjs';
import { FormControl } from '@angular/forms';

const compare = (v1: string | number, v2: string | number) => (v1 < v2 ? -1 : v1 > v2 ? 1 : 0);

@Component({
  selector: 'app-custom-table',
  templateUrl: './custom-table.component.html',
  styleUrls: ['./custom-table.component.scss']
})
export class CustomTableComponent implements OnInit {
  @ViewChildren(SortableDirective) headers!: QueryList<SortableDirective>;
  @Input() tableSetting!: CustomTableSetting;
  @Input() formInputs!: CustomFormInput[];
  tableData: TableData[] = [];
  IDENTIFIER = IDENTIFIER;
  tableData$!: Observable<TableData[]>;
  page: number = 1;
	pageSize: number = 5;
	collectionSize!: number;

  searchTerm: string = ''

  constructor(private customService: CustomCollapseService) {
    this.customService.shareData.subscribe(() => {
      this.getTableData();
    })
  }

  ngOnInit(): void {
    this.getTableData();
  }


  getTableData() {
    this.tableData = this.customService.getData(this.tableSetting.type!);
    this.tableData.forEach((data) => {
      data['isEdit'] = false;
    });
    this.collectionSize = this.tableData.length;
  }

  onSort({ column, direction }: SortEvent) {
    this.headers.forEach((header) => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    if (direction === '' || column === '') {
			this.getTableData();
		} else {
			this.tableData = [...this.tableData].sort((a, b) => {
				const res = compare(a[column], b[column]);
				return direction === 'asc' ? res : -res;
			});
		}
  }

  editData(data: any) {
    data.isEdit = true;
  }

  updateData(data: any) {
    this.customService.updateData(data, this.tableSetting.type!);
    data.isEdit = false;
    this.customService.shareData.next();
  }

  deleteData(data: any) {
    this.customService.deleteData(data, this.tableSetting.type!);
    this.customService.shareData.next();
  }

  cancelData(data: any) {
    data.isEdit = false;
  }
}
