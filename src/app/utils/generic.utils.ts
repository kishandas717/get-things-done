export const getRandomBgColor = () => {
    const bgColors = ['#845EC2', '#D65DB1', '#FF6F91', '#FF9671', '#FFC75F', '#F9F871', '#00C9A7', '#F3C5FF'];
    const bgColor = Math.floor(Math.random() * bgColors.length);
    return bgColors[bgColor];
}

export const hexToRGB = (hexCode: string) => {
    let r!: string, g!: string, b!: string;
  
    if (hexCode.length == 4) {
      r = "0x" + hexCode[1] + hexCode[1];
      g = "0x" + hexCode[2] + hexCode[2];
      b = "0x" + hexCode[3] + hexCode[3];
  
    } else if (hexCode.length == 7) {
      r = "0x" + hexCode[1] + hexCode[2];
      g = "0x" + hexCode[3] + hexCode[4];
      b = "0x" + hexCode[5] + hexCode[6];
    }
    
    return "rgb("+ +r + "," + +g + "," + +b + ",0.2)";
  }