import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appContenteditableMaxLength]'
})
export class ContenteditableMaxLengthDirective {
  constructor(private el: ElementRef) { }

  @HostListener('keydown', ['$event']) onKeyDown(event: KeyboardEvent) {
    const maxLength = parseInt(this.el.nativeElement.getAttribute('maxlength'));
    if (this.el.nativeElement.innerText.length >= maxLength && event.keyCode !== 8) {
      event.preventDefault();
    }
  }

  @HostListener('input', ['$event']) onInput(event: Event) {
    const maxLength = parseInt(this.el.nativeElement.getAttribute('maxlength'));
    if (this.el.nativeElement.innerText.length > maxLength) {
      this.el.nativeElement.innerText = this.el.nativeElement.innerText.slice(0, maxLength);
    }
  }
}