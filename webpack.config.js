const CompressionPlugin = require('compression-webpack-plugin');

module.exports = {
  mode: 'production',
  optimization: {
    usedExports: true,
    minimize: true,
    sideEffects: true,
    concatenateModules: true,
    splitChunks: {
      chunks: 'all',
      minSize: 10000,
      maxSize: 250000
    }
  },
  plugins: [
    new CompressionPlugin()
  ]
};